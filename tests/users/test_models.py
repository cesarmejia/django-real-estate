import pytest


def test_user_str(base_user):
    assert str(base_user) == base_user.username


def test_user_short_name(base_user):
    assert base_user.short_name == base_user.username


def test_user_full_name(base_user):
    full_name = f"{base_user.first_name.title()} {base_user.last_name.title()}"
    assert base_user.full_name == full_name


def test_base_user_email_is_normalized(base_user):
    assert base_user.email == base_user.email.lower()


def test_super_user_email_is_normalized(super_user):
    assert super_user.email == super_user.email.lower()


def test_super_user_is_not_staff(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(is_superuser=True, is_staff=False)
    assert str(err.value) == "Superusers must have is_staff=True."


def test_super_user_is_not_superuser(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(is_superuser=False, is_staff=True)
    assert str(err.value) == "Superusers must have is_superuser=True."


def test_create_user_with_no_email(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(email=None)
    assert str(err.value) == "Users must have an email."


def test_create_user_with_no_username(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(username=None)
    assert str(err.value) == "Users must have a username."


def test_create_user_with_no_firstname(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(first_name=None)
    assert str(err.value) == "Users must have a first name."


def test_create_user_with_no_lastname(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(last_name=None)
    assert str(err.value) == "Users must have a last name."


def test_create_superuser_with_no_email(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(email=None, is_superuser=True)
    assert str(err.value) == "Users must have an email."


def test_create_superuser_with_no_password(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(password=None, is_superuser=True)
    assert str(err.value) == "Superusers must have a password."


def test_user_email_incorrect(user_factory):
    with pytest.raises(ValueError) as err:
        user_factory.create(email="invalid_email")
    assert str(err.value) == "You must provide a valid email address."
