import pytest


def test_profile_str(profile):
    assert str(profile) == f"Profile: {profile.user.username}"
