from django.contrib.auth import get_user_model
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from apps.profiles.models import Profile

from .models import Rating

User = get_user_model()


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def create_rating(request, profile_id):
    agent_profile = Profile.objects.get(id=profile_id, is_agent=True)
    rater = request.user
    data = request.data
    agent_user = User.objects.get(pkid=agent_profile.user.pkid)
    if agent_user.email == rater.email:
        formatted_response = {"message": "You can't rate yourself"}
        return Response(formatted_response, status=status.HTTP_403_FORBIDDEN)
    has_reviewed = agent_profile.ratings.filter(rater=rater).exists()
    if has_reviewed:
        formatted_response = {"message": "You have already rated this agent"}
        return Response(formatted_response, status=status.HTTP_400_BAD_REQUEST)
    elif data["range"] == 0:
        formatted_response = {"message": "Please select a rating"}
        return Response(formatted_response, status=status.HTTP_400_BAD_REQUEST)
    else:
        Rating.objects.create(rater=rater, agent=agent_profile, range=data["range"], comment=data["comment"])
        ratings = agent_profile.ratings.all()
        agent_profile.num_reviews = len(ratings)
        total = 0
        for i in ratings:
            total += i.range
        agent_profile.rating = round(float(total / len(ratings)), 2)
        agent_profile.save()

        return Response("Review submitted", status=status.HTTP_201_CREATED)
