from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.common.models import BaseModel
from apps.profiles.models import Profile

User = get_user_model()


class Range(models.IntegerChoices):
    """Rating range."""

    RATING_1 = 1, _("Poor")
    RATING_2 = 2, _("Fair")
    RATING_3 = 3, _("Good")
    RATING_4 = 4, _("Great")
    RATING_5 = 5, _("Excellent")


class Rating(BaseModel):
    """Rating model."""

    rater = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        related_name="ratings",
        verbose_name=_("User providing the ratings"),
    )
    agent = models.ForeignKey(
        Profile, related_name="ratings", on_delete=models.SET_NULL, verbose_name=_("Agent being rated"), null=True
    )
    range = models.SmallIntegerField(
        choices=Range.choices,
        verbose_name=_("Rating"),
        default=Range.RATING_3,
        help_text=_("1=Poor, 2=Fair, 3=Good, 4=Great, 5=Excellent"),
    )
    comment = models.TextField(verbose_name=_("Comment"), blank=True)

    class Meta:
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")
        unique_together = ("rater", "agent")

    def __str__(self):
        return f"{self.rater} rated {self.agent}: {self.range}/5"
