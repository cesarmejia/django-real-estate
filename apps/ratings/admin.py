from django.contrib import admin

from .models import Rating


class RatingAdmin(admin.ModelAdmin):
    list_display = ("rater", "agent", "range")
    list_filter = ("rater", "agent", "range")


admin.site.register(Rating, RatingAdmin)
