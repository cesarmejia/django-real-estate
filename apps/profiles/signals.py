import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from real_estate.settings.base import AUTH_USER_MODEL

from .models import Profile

logger = logging.getLogger(__name__)


@receiver(post_save, sender=AUTH_USER_MODEL)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        logger.info(f"Profile created for user: {instance}")


@receiver(post_save, sender=AUTH_USER_MODEL)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
    logger.info(f"Profile saved for user: {instance}")
