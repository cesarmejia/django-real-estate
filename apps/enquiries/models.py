from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from apps.common.models import BaseModel


class Enquiry(BaseModel):
    name = models.CharField(verbose_name=_("Your Name"), max_length=100)
    phone_number = PhoneNumberField(verbose_name=_("Phone Number"), max_length=30, default="+13235554242")
    email = models.EmailField(verbose_name=_("Email"), max_length=100)
    message = models.TextField(verbose_name=_("Message"), max_length=500)
    subject = models.CharField(_("Subject"), max_length=100)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _("Enquiry")
        verbose_name_plural = _("Enquiries")
