import random
import string

from autoslug import AutoSlugField
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField

from apps.common.models import BaseModel

User = get_user_model()


class PropertyPublishedManager(models.Manager):
    def get_queryset(self):
        return super(PropertyPublishedManager, self).get_queryset().filter(is_published=True)


class Property(BaseModel):
    class AdvertType(models.TextChoices):
        RENT = "Rent", _("Rent")
        SALE = "Sale", _("Sale")
        AUCTION = "Auction", _("Auction")

    class PropertyType(models.TextChoices):
        APARTMENT = "Apartment", _("Apartment")
        HOUSE = "House", _("House")
        OFFICE = "Office", _("Office")
        WAREHOUSE = "Warehouse", _("Warehouse")
        COMMERCIAL = "Commercial", _("Commercial")
        OTHER = "Other", _("Other")

    user = models.ForeignKey(
        User, related_name="properties", on_delete=models.DO_NOTHING, verbose_name=_("Agent, Seller or Buyer")
    )
    title = models.CharField(max_length=255, verbose_name=_("Property Title"))
    slug = AutoSlugField(populate_from="title", unique=True, verbose_name=_("Slug"), always_update=True)
    ref_code = models.CharField(max_length=255, unique=True, verbose_name=_("Reference Code"), blank=True)
    description = models.TextField(default=_("Enter a description"), verbose_name=_("Description"))
    country = CountryField(verbose_name=_("Country"), default="US", blank_label="(Select country)")
    city = models.CharField(max_length=180, verbose_name=_("City"), default="Los Angeles")
    postal_code = models.CharField(max_length=100, verbose_name=_("Postal Code"), default="90210")
    street_address = models.CharField(max_length=150, verbose_name=_("Street Address"), default="123 Main St")
    property_number = models.IntegerField(
        verbose_name=_("Property Number"), default=111, validators=[MinValueValidator(1)]
    )
    price = models.DecimalField(verbose_name=_("Price"), max_digits=10, decimal_places=2, default=0.00)
    tax = models.DecimalField(
        verbose_name=_("Property Tax"),
        max_digits=10,
        decimal_places=5,
        default=0.15000,
        help_text=_("Tax in decimal format"),
    )
    floor_area = models.DecimalField(verbose_name=_("Floor Area (m2)"), max_digits=10, decimal_places=2, default=0.00)
    floors = models.IntegerField(verbose_name=_("Number of Floors"), default=1, validators=[MinValueValidator(1)])
    bedrooms = models.IntegerField(verbose_name=_("Number of Bedrooms"), default=0, validators=[MinValueValidator(0)])
    bathrooms = models.DecimalField(
        verbose_name=_("Number of Bathrooms"),
        validators=[MinValueValidator(0)],
        default=1.0,
        decimal_places=1,
        max_digits=4,
    )
    advert_type = models.CharField(
        verbose_name=_("Advertisement Type"), max_length=20, choices=AdvertType.choices, default=AdvertType.RENT
    )
    property_type = models.CharField(
        verbose_name=_("Property Type"), max_length=20, choices=PropertyType.choices, default=PropertyType.APARTMENT
    )
    cover_photo = models.ImageField(
        verbose_name=_("Main Photo"), upload_to="properties/", blank=True, null=True, default="/house_sample.jpg"
    )
    photo1 = models.ImageField(
        verbose_name=_("Photo 1"), upload_to="properties/", blank=True, null=True, default="/interior_sample.jpg"
    )
    photo2 = models.ImageField(
        verbose_name=_("Photo 2"), upload_to="properties/", blank=True, null=True, default="/interior_sample.jpg"
    )
    photo3 = models.ImageField(
        verbose_name=_("Photo 3"), upload_to="properties/", blank=True, null=True, default="/interior_sample.jpg"
    )
    photo4 = models.ImageField(
        verbose_name=_("Photo 4"), upload_to="properties/", blank=True, null=True, default="/interior_sample.jpg"
    )
    is_published = models.BooleanField(verbose_name=_("Published"), default=False)
    views = models.PositiveIntegerField(verbose_name=_("Total Views"), default=0)

    objects = models.Manager()
    published = PropertyPublishedManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Property")
        verbose_name_plural = _("Properties")

    def save(self, *args, **kwargs):
        self.title = str.title(self.title)
        self.description = str.capitalize(str(self.description))  # Avoids crashing when saving
        self.ref_code = "".join(random.choices(string.ascii_uppercase + string.digits, k=10))
        super(Property, self).save(*args, **kwargs)

    @property
    def final_property_price(self):
        tax_amount = round(self.price * self.tax, 2)
        price_with_tax = float(round(self.price + tax_amount, 2))
        return price_with_tax


class PropertyViews(BaseModel):
    ip = models.CharField(max_length=255, verbose_name=_("IP Address"))
    property = models.ForeignKey(
        Property, on_delete=models.CASCADE, verbose_name=_("Property"), related_name="property_views"
    )

    class Meta:
        verbose_name = _("Property View")
        verbose_name_plural = _("Property Views")

    def __str__(self):
        return f"{self.property.title}: {self.property.views} View(s)"
