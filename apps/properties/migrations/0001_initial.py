# Generated by Django 3.2.7 on 2021-12-19 08:50

import autoslug.fields
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Property',
            fields=[
                ('pkid', models.BigAutoField(editable=False, primary_key=True, serialize=False)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=255, verbose_name='Property Title')),
                ('slug', autoslug.fields.AutoSlugField(always_update=True, editable=False, populate_from='title', unique=True, verbose_name='Slug')),
                ('ref_code', models.CharField(blank=True, max_length=255, unique=True, verbose_name='Reference Code')),
                ('description', models.TextField(default='Enter a description', verbose_name='Description')),
                ('country', django_countries.fields.CountryField(default='US', max_length=2, verbose_name='Country')),
                ('city', models.CharField(default='Los Angeles', max_length=180, verbose_name='City')),
                ('postal_code', models.CharField(default='90210', max_length=100, verbose_name='Postal Code')),
                ('street_address', models.CharField(default='123 Main St', max_length=150, verbose_name='Street Address')),
                ('property_number', models.IntegerField(default=111, validators=[django.core.validators.MinValueValidator(1)], verbose_name='Property Number')),
                ('price', models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='Price')),
                ('tax', models.DecimalField(decimal_places=5, default=0.15, help_text='Tax in decimal format', max_digits=10, verbose_name='Property Tax')),
                ('floor_area', models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='Floor Area (m2)')),
                ('floors', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)], verbose_name='Number of Floors')),
                ('bedrooms', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)], verbose_name='Number of Bedrooms')),
                ('bathrooms', models.DecimalField(decimal_places=1, default=1.0, max_digits=4, validators=[django.core.validators.MinValueValidator(0)], verbose_name='Number of Bathrooms')),
                ('advert_type', models.CharField(choices=[('Rent', 'Rent'), ('Sale', 'Sale'), ('Auction', 'Auction')], default='Rent', max_length=20, verbose_name='Advertisement Type')),
                ('property_type', models.CharField(choices=[('Apartment', 'Apartment'), ('House', 'House'), ('Office', 'Office'), ('Warehouse', 'Warehouse'), ('Commercial', 'Commercial'), ('Other', 'Other')], default='Apartment', max_length=20, verbose_name='Property Type')),
                ('cover_photo', models.ImageField(blank=True, default='/house_sample.jpg', null=True, upload_to='properties/', verbose_name='Main Photo')),
                ('photo1', models.ImageField(blank=True, default='/interior_sample.jpg', null=True, upload_to='properties/', verbose_name='Photo 1')),
                ('photo2', models.ImageField(blank=True, default='/interior_sample.jpg', null=True, upload_to='properties/', verbose_name='Photo 2')),
                ('photo3', models.ImageField(blank=True, default='/interior_sample.jpg', null=True, upload_to='properties/', verbose_name='Photo 3')),
                ('photo4', models.ImageField(blank=True, default='/interior_sample.jpg', null=True, upload_to='properties/', verbose_name='Photo 4')),
                ('is_published', models.BooleanField(default=False, verbose_name='Published')),
                ('views', models.PositiveIntegerField(default=0, verbose_name='Total Views')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='properties', to=settings.AUTH_USER_MODEL, verbose_name='Agent, Seller or Buyer')),
            ],
            options={
                'verbose_name': 'Property',
                'verbose_name_plural': 'Properties',
            },
        ),
        migrations.CreateModel(
            name='PropertyViews',
            fields=[
                ('pkid', models.BigAutoField(editable=False, primary_key=True, serialize=False)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('ip', models.CharField(max_length=255, verbose_name='IP Address')),
                ('property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='property_views', to='properties.property', verbose_name='Property')),
            ],
            options={
                'verbose_name': 'Property View',
                'verbose_name_plural': 'Property Views',
            },
        ),
    ]
